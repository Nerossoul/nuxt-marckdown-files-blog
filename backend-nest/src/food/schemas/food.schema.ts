import * as mongoose from 'mongoose';

export const FoodSchema = new mongoose.Schema({
  Name: { type: String },
  fullname: { type: String },
  LUTEIN: { type: Number },
  kalori: { type: Number },
  vitN: { type: Number },
  GIN: { type: Number },
  belok: { type: Number },
  tuk: { type: Number },
  satva: { type: Number },
  K: { type: Number },
  Ca: { type: Number },
  Na: { type: Number },
  Mg: { type: Number },
  P: { type: Number },
  Fe: { type: Number },
  I: { type: Number },
  S: { type: Number },
  Mn: { type: Number },
  Cu: { type: Number },
  Mo: { type: Number },
  H2O: { type: Number },
  Si: { type: Number },
  Co: { type: Number },
  Se: { type: Number },
  Cl: { type: Number },
  Cr: { type: Number },
  F: { type: Number },
  Zn: { type: Number },
  vitA: { type: Number },
  Alum: { type: Number },
  Olovo: { type: Number },
  vitC: { type: Number },
  vitD: { type: Number },
  vitE: { type: Number },
  vitH: { type: Number },
  vitK: { type: Number },
  B1: { type: Number },
  B2: { type: Number },
  PP: { type: Number },
  B4: { type: Number },
  B5: { type: Number },
  B6: { type: Number },
  B12: { type: Number },
  B9: { type: Number },
  losa: { type: Number },
  spez: { type: Number },
  analogi: { type: String },
  file: { type: String },
  prop: { type: Number },
  porz: { type: String },
  B15: { type: Number },
  Bor: { type: Number },
  Brom: { type: Number },
  V: { type: Number },
  As: { type: Number },
  Ni: { type: Number },
  Cd: { type: Number },
  Pbsvin: { type: Number },
  Ba: { type: Number },
  Zesi: { type: Number },
  photo: { type: String },
  code: { type: String },
  addres: { type: String },
  Ingredients: { type: String },
  vitU: { type: Number },
  betain: { type: Number },
  percent: { type: Number },
  Stronz: { type: Number },
  Rb: { type: Number },
  Zr: { type: Number },
  Tal: { type: Number },
  Liti: { type: Number },
  Iridi: { type: Number },
  deistvie: { type: String },
});
