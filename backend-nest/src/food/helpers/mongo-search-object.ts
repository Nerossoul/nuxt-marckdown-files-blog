import { getEEarray } from './ee-helper';

export const createSearchObject = text => {
  const texts = getEEarray(text);
  const searchStringObjects = texts.map(name => ({
    Name: {
      $regex: new RegExp('^' + name + '', 'i'),
    },
  }));
  console.log(searchStringObjects);

  return { $or: searchStringObjects };
};
