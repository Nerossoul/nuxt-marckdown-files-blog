export const getEEarray = (text): Array<string> => {
  const letters = text.toLowerCase().split('');

  const reducer = (words, curreltLetter, index) => {
    const newWords = [...words];
    if (curreltLetter === 'е') {
      words.forEach(word => {
        const newWord = word.slice(0, index) + 'ё' + word.slice(index + 1);
        newWords.push(newWord);
      });
    }
    if (curreltLetter === 'ё') {
      words.forEach(word => {
        const newWord = word.slice(0, index) + 'е' + word.slice(index + 1);
        newWords.push(newWord);
      });
    }
    return newWords;
  };

  return letters.reduce(reducer, [text]);
};
