import { Controller, Get, Post, Body, Param } from '@nestjs/common';
import { FoodService } from './food.service';

@Controller('api/food')
export class FoodController {
  constructor(private readonly foodService: FoodService) {}

  @Get('')
  test() {
    return 'TO DO';
  }

  @Get('search/:name')
  search(@Param('name') name) {
    return this.foodService.search(name);
  }

  @Post('')
  addProducts(@Body() products) {
    return this.foodService.addProducts(products);
  }
}
