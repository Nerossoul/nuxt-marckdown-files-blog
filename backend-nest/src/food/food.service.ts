import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { IFood } from './intefaces/food.interface';

import { createSearchObject } from './helpers/mongo-search-object';

@Injectable()
export class FoodService {
  constructor(
    @InjectModel('Food')
    private readonly FoodModel: Model<IFood>,
  ) {}

  async test() {
    const newObject = { Id: '' };
    let newSparkProperty = new this.FoodModel(newObject);
    return await newSparkProperty.save();
  }

  async addProducts(products) {
    let results = [];
    for (const product of products) {
      const existingProduct = await this.getProductFromBaseByName(product.Name);
      if (existingProduct === null) {
        let newSparkProperty = new this.FoodModel(product);
        let id = await newSparkProperty.save();
        results.push(id);
        console.log('Добавляем ', product.Name);
      } else {
        await existingProduct.updateOne(product);
        const id = await existingProduct.save();
        results.push(id);
        console.log('Обновляем ', product.Name);
      }
    }
    return results;
  }

  async getProductFromBaseByName(Name): Promise<IFood> {
    const propertyData = await this.FoodModel.findOne({
      Name: Name,
    });
    return propertyData;
  }

  async search(name) {
    const searchObject = createSearchObject(name);
    const products = await this.FoodModel.find(searchObject);
    return products;
  }
}
