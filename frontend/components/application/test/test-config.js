export const quickTestConfig = [
  {
    name: 'the_upper_pressure',
    label: 'верхнее давление',
    min: 80,
    max: 200,
    step: 1,
  },
  {
    name: 'lower_pressure',
    label: 'нижнее давление',
    min: 40,
    max: 130,
    step: 1,
  },
  {
    name: 'heart_rate_on_the_device',
    label: 'пульс по прибору',
    min: 40,
    max: 200,
    step: 1,
  },
  {
    name: 'temperature',
    label: 'температура тела',
    min: 35,
    max: 42,
    step: 0.1,
  },

  {
    name: 'open_air_time',
    label: 'сколько часов ежедневно проводите на свежем воздухе',
    min: 0,
    max: 10,
    step: 1,
  },
  {
    name: 'heart_rate',
    label: 'пульс в покое после завтрака',
    min: 50,
    max: 150,
    step: 1,
  },
  {
    name: 'health',
    label: 'субъективная личная оценка состояния здоровья',
    min: 0,
    max: 5,
    step: 1,
  },
  {
    name: 'purpose',
    label:
      'цель выхода на улицу: 1 поездка в транспорте_ 2 пешая прогулка_ 3 работа на даче_ 4 ходьба с ускорением более 25 минут_ 5 марш-бросок на 5 км',
    min: 0,
    max: 5,
    step: 1,
  },
  {
    name: 'mood',
    label: 'оценка настроения в данный момент',
    min: 1,
    max: 5,
    step: 1,
  },
]

export const fullTestConfig = []
