import footer from './footer'
import indexPageHead from './index-page-head'

export default {
  changeLanguagePost: 'Пост доступен на русском',
  soonLanguagePost: 'Скоро будет доступен',
  comeBack: 'Назад',
  indexPageHead: indexPageHead,
  posts: 'Статьи',
  home: 'Home',
  footer: footer
}
