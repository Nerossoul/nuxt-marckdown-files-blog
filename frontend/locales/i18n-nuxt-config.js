const I18N = {
  useCookie: false,
  alwaysRedirect: true,
  locales: [
    {
      code: 'ru',
      iso: 'ru-RU',
      name: 'Русский',
      file: 'ru/index.js'
    }
  ],
  lazy: true,
  seo: false,
  langDir: '/locales/',
  defaultLocale: 'ru',
  parsePages: false
}

module.exports = {
  I18N
}
