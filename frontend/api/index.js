import axios from 'axios'

class Api {
  constructor() {
    this.apiClient = axios.create({
      baseURL: `/api/`,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
  }

  food = {
    getProducts: () => {
      return this.apiClient.get(`food/`)
    },
    addProducts: (data) => {
      console.log(data)
      return this.apiClient.post('food/', data)
    },
    search: (name) => {
      if (name !== '') {
        return this.apiClient.get(`food/search/${name}`)
      } else {
        return { data: [] }
      }
    },
  }
  // propertySearch = {
  //     find: (data) => {
  //         return this.apiClient.post('property-search/', data)
  //     },
  //     count: (data) => {
  //         return this.apiClient.post('property-search/count', data)
  //     },
  //     updatePhotos: (data) => {
  //         return this.apiClient.post('property-search/updatephotos', data)
  //     },
  //     getProperty:
  //         (propertyId) => {
  //             return this.apiClient.get(`property-search/id/${propertyId}`)
  //         },
  // }
}

export default new Api()
