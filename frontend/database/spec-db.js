const specification = `GIN,belok,tuk,satva,K,Ca,Na,Mg,P,Fe,I,S,Mn,Cu,Mo,H2O,Si,Co,Se,Cl,Cr,F,Zn,vitA,vitC,vitD,vitE,vitH,vitK,B1,B2,PP,B4,B5,B6,B12,B9,losa,spez,B15,Bor,Brom,V,As,Ni,kalori
ГЛИКЕМИЧЕСКИЙ ИНДЕКС,БЕЛКИ,ЖИРЫ,УГЛЕВОДЫ,Калий (мг в 100г продукта),Кальций (мг в 100г продукта),   Na,Магний (мг в 100г продукта),Фосфор,Железо (мкг в 100г продукта),Йод(мкг в 100г продукта),Сера мг (суточная н.995мг),  Марганец Mn(мкг в 100г пр.),  Медь(мкг в 100г пр.),Молибден Мо(мкг в 100г пр.),ВОДА,Кремний (мг в 100г пр.) с.н.30 мг,Кобальт (мкг в 100г п.),  Селен (мкг в 100г пр.) с.н.54мкг,ХЛОР,  Хром(мкг в 100г п.) c.потр.50мкг,ФТОР (не превышать 0 3г Х ВЕС) с.н.=3000мкг,ЦИНК(мкг в 100г продукта),вит. А (сут. н. 1000мкг), аскорбин. кислота С (мг)          ,  D (мкг),    E (мг), H (В7)  биотин (с.нор. 50мкг),  K (мкг),      B1 (тиамин  мг),    В2 (рибофлавин) мГ, РР(ниацин  никотиновая кислота) -вит.B3 (мг), B4 ХОЛИН мг,     В5  ПАНТЕОНОВАЯ КИСЛОТА (мг),       B6 пиродоксин (мг),B12 циа- нокобаламин (мкг),фолиевая кислота В9 (мкг),пище- вые волокна,ИЗМЕНЕНИЕ КИСЛОТНО-ЩЕЛ. РАВН., В15 (ПАН- ГАМОВАЯ КИСЛОТА),   БОР        (1-3 мГ),БРОМ (0 5-2 мг),ВАНАДИЙ (6-63мкг),МЫШЬЯК,НИКЕЛЬ (100- 300мкг),kKal в 100г
10.0,30.0,50.0,175.0,2500.0,900.0,550.0,420.0,800.0,10000.0,150.0,995.0,2000.0,1000.0,70.0,2000.0,30.0,10.0,55.0,1000.0,35.0,500.0,12000.0,900.0,150.0,10.0,15.0,50.0,20.0,1.5,1.8,17.0,40.0,8.0,2.0,3.0,250.0,20.0,7.36,1.0,400.0,300.0,3.0,1.0,3.0,1300.0
edin,Gram,Gram,Gram,mGram,mGram,mGram,mGram,mGram,mkGram,mkGram,mGram,mkGram,mkGram,mkGram,Gram,mGram,mkGram,mkGram,mGram,mkGram,mkGram,mkGram,mkGram,mGram,mkGram,mGram,mkGram,mkGram,mGram,mGram,mGram,mGram,mGram,mGram,mkGram,mkGram,Gram,edin,mGram,mGram,mGram,mkGram,mkGram,mkGram,kKal`

function csvTojs(csv) {
  var lines = csv.split('\n')
  var result = []
  var headers = lines[0].split(',')

  for (var i = 1; i < lines.length; i++) {
    var obj = {}

    var row = lines[i],
      queryIdx = 0,
      startValueIdx = 0,
      idx = 0

    if (row.trim() === '') {
      continue
    }

    while (idx < row.length) {
      /* if we meet a double quote we skip until the next one */
      var c = row[idx]

      if (c === '"') {
        do {
          c = row[++idx]
        } while (c !== '"' && idx < row.length - 1)
      }

      if (
        c === ',' ||
        /* handle end of line with no comma */ idx === row.length - 1
      ) {
        /* we've got a value */
        var value = row.substr(startValueIdx, idx - startValueIdx).trim()

        /* skip first double quote */
        if (value[0] === '"') {
          value = value.substr(1)
        }
        /* skip last comma */
        if (value[value.length - 1] === ',') {
          value = value.substr(0, value.length - 1)
        }
        /* skip last double quote */
        if (value[value.length - 1] === '"') {
          value = value.substr(0, value.length - 1)
        }

        var key = headers[queryIdx++]
        obj[key] = value
        startValueIdx = idx + 1
      }

      ++idx
    }

    result.push(obj)
  }
  return result
}

const specJS = csvTojs(specification)
const specObj = Object.assign(
  {},
  ...Object.keys(specJS[0]).map((key) => {
    return {
      [key]: {
        name: specJS[0][key],
        minQuantity: specJS[1][key],
        units: specJS[2][key],
      },
    }
  })
)

// меняем значение гликемического индекса
specObj['GIN']['minQuantity'] = 30

export default specObj
